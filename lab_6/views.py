from django.shortcuts import render


response = {'author': 'Nadia Nabila Anjani'}
# Create your views here.
def index(request):
    return render(request, 'lab_6/lab_6.html', response)

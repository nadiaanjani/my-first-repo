from django.shortcuts import render


response = {'author': 'Nadia Nabila Anjani'}
# Create your views here.
def index(request):
    return render(request, 'lab_8/lab_8.html', response)
